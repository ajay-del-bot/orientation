# Gitlab Summary:

### Definition:
GitLab is a web-based DevOps lifecycle tool that provides a Git-repository manager providing wiki, issue-tracking and continuous integration/continuous deployment pipeline features, using an open-source license, developed by GitLab Inc.

### Summary:
1. Gitlab has built-in solutions for all the stages involved in software developement.
2. For instance, it provides **Issue Tracker** and **Issue Board** for an effective discussion and planning.
3. It also provides features of _review_ and _staging_ to deploy our code in the environment.
4. Conclusively, it is fast, efficient, productive and easy to use.

###### Preview of GitLab:

![image](https://gitlab.com/kanchitank/orientation/-/raw/master/images/3.jpg)