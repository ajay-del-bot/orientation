# Git Summary

### Definition:
Git is a distributed version-control system for tracking changes in source code during software development. It is designed for coordinating work among programmers

### Commands on Git:
1. **Create a new repository**:  
`git init`
2. **Checkout the repository**:  
`git clone /path_to_repository/`
3. **Add and Commit**:  
`git add file_name`  
`git commit -m "Commit message"`
4. **Pushing changes**:  
`git push origin master`    
Change master to whatever branch you want to push your changes to
5. **Branching**:  
`git checkout -b branch_name`  
Switch back to master:  
`git checkout master`  
6. **Pushing**:  
`git push origin branch`

### References:
[git - a simple guide](https://rogerdudler.github.io/git-guide/)